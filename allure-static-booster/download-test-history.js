/* eslint-disable */

// job to download latest successful generated report
const request = require("sync-request");
const {execSync} = require("child_process");
const fs = require("fs");
const jp = require("jsonpath");

const apiUrl = process.argv[2];
const projectId = process.argv[3];
const projectToken = process.argv[4];
const ciPipelineId = process.argv[5];

const projectApiPath = `${apiUrl}/projects/${projectId}`;
const pastReport = "pastReport";

const attemptsPerMinute = 6;
const delayBetweenCalls = 60 / attemptsPerMinute;
const minutesToCheckPagesJob = 5;
const minutesToCheckRunningPipeline = 50;

// Run
downloadHistory();

function downloadHistory() {
  let isDownloadSuccess = "false";

  console.log("Fetching latest pipelines for master");
  const pipelinesList = getGitlabCIapi("/pipelines?ref=master&order_by=id");
  //we need only pipelines started before current
  let spliceIndex = -1;
  for (let i = 0; i < pipelinesList.length; i++) {
    if (pipelinesList[i].id == ciPipelineId) { //list and number values compare
      spliceIndex = i;
    }
  }
  pipelinesList.splice(0, spliceIndex + 1);

  let pipeline;
  let pipelineId;

  for (let i = 0; i < pipelinesList.length; i++) {
    pipeline = pipelinesList[i];
    pipelineId = pipeline.id;
    console.log(`Fetching jobs for pipeline: ${pipelineId}, pipeline is ${pipeline.status}`);
    let counter = 1;
    let downloadStatus;
    do {
      console.log(`Pipeline ${pipelineId} ${counter}'s attempt`);
      downloadStatus = downloadFromPagesJob(pipelineId);
      console.log(`Current download status for pipeline ${pipelineId} is ${downloadStatus}`);
      if (pipeline.status === "success" || pipeline.status === "failed") {
        isDownloadSuccess = downloadStatus;
        break;
      } else if (pipeline.status === "canceled" || pipeline.status === "skipped") {
        console.log(`Skipping pipeline ${pipelineId} pipeline due to status: ${pipeline.status}`);
        break;
      } else {
        if (downloadStatus === "pages_was_created" || downloadStatus === "test_run_stage") {
          console.log(`Pipeline ${pipelineId}, pipeline is ${pipeline.status}, waiting for pages job appear...`);
          pipeline = getGitlabCIapi(`/pipelines/${pipelineId}`);
          execSync(`sleep ${delayBetweenCalls}s`);
          counter++;
        } else {
          isDownloadSuccess = downloadStatus;
          break;
        }
      }
    } while (counter < (minutesToCheckRunningPipeline * attemptsPerMinute));

    if (isDownloadSuccess === "pages_was_downloaded") {
      break;
    }
  }

  if (isDownloadSuccess !== "pages_was_downloaded") {
    throw new Error(`Valid reports was not found for last ${pipelinesList.length} pipelines`);
  }
  console.log(`Latest successful report was downloaded from ${pipelineId} pipeline`);
}

function getGitlabCIapi(requestApiPath) {
  const res = request("GET", projectApiPath + requestApiPath, {
    headers: {
      "PRIVATE-TOKEN": projectToken
    }
  });
  return JSON.parse(res.getBody("utf8"));
}

function downloadFromPagesJob(pipelineId) {
  let pipelineJobsList = getGitlabCIapi(`/pipelines/${pipelineId}/jobs`);
  let pagesJob = jp.query(pipelineJobsList, `$..[?(@.name=="pages")]`)[0];

  if (pagesJob != null) {
    console.log(`Fetching pages job ${pagesJob.id} for pipeline ${pipelineId}...`);
    let counter = 0;
    do {
      if (pagesJob.status === "failed" || pagesJob.status === "canceled" || pagesJob.status === "skipped") {
        console.log(`Pages job ${pagesJob.id} for pipeline ${pipelineId} is failed, jump to next pipeline`);
        return "pages_not_created";
      } else if (pagesJob.status === "success") {
        console.log(`Pages job ${pagesJob.id} status is success, downloading...`);
        execSync(`curl --location --output "${pastReport}.zip" ` +
          `--header "PRIVATE-TOKEN: ${projectToken}" ` +
          `--request GET "${projectApiPath}/jobs/${pagesJob.id}/artifacts"`);
        console.log("Wait for archive processing");
        fs.renameSync(`${pastReport}.zip`, `../${pastReport}.zip`);
        return "pages_was_downloaded";
      } else if (pagesJob.status === "created") {
        console.log(`Pages job ${pagesJob.id} for pipeline ${pipelineId} is created only, waiting other stages to complete`);
        return "pages_was_created";
      } else {
        console.log(`Pages job status is ${pagesJob.status} waiting for success...`);
        pipelineJobsList = getGitlabCIapi(`/pipelines/${pipelineId}/jobs`);
        pagesJob = jp.query(pipelineJobsList, `$..[?(@.name=="pages")]`)[0];
        execSync(`sleep ${delayBetweenCalls}s`);
      }
      counter++;
    } while (counter < (attemptsPerMinute * minutesToCheckPagesJob));
  } else {
    let testJobs = jp.query(pipelineJobsList, `$..[?(@.stage=="test")]`)[0];
    if (testJobs != null) {
      return "test_run_stage";
    }
    console.log(`Pages job for pipeline ${pipelineId} wasn't created`);
  }

  return "pages_not_created";
}
