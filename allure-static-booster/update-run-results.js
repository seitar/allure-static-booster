/* eslint-disable */

// script to generate and update online hosted report (deploy pages job)
const fs = require("fs");
const {execSync} = require("child_process");
const jsdom = require("jsdom");
const {JSDOM} = jsdom;

const envString = fs.readFileSync("../allure-results/environment.properties").toString();
const envJson = JSON.parse("{\"" + envString.replace(/\n/g, "\", \"").replace(/=/g, "\": \"") + "\"}");
let summaryJson;

const scopesJson = JSON.parse(fs.readFileSync("./test-scopes.json"));
const runScope = envJson.REGRESSION;
const customRuns = "custom_runs";
const runScopes = Object.keys(scopesJson["runScopes"]);
const testScopes = Object.keys(scopesJson["runScopes"][runScope]).concat([customRuns]);
let isDefaultScope = false;
let testScope;
let allureReportDir;

const versionId = "id=\"version-1.0.1\""; //placed in header of report_template.html

const pastReport = "pastReport";

// Run
updateRunResults();

function updateRunResults() {
  getTestScope();
  copyDownloadedHistory();
  generateAllureReport();
  modifyAllureReport();
  updateReportHtml();
}

function getTestScope() {
  let scopeObject = {
    "ENV": envJson.ENV,
    "EXPERIMENT": envJson.EXPERIMENT,
  };

  scopeObject = JSON.stringify(scopeObject);
  console.log("Test scope object: " + scopeObject);
  const defaultScopeObject = scopesJson.default;

  let isCustomScope = true;
  Object.keys(scopesJson["runScopes"][runScope]).forEach(scope => {
    let templateScope = JSON.stringify(Object.assign(defaultScopeObject, scopesJson["runScopes"][runScope][scope]));
    if (scopeObject === templateScope) {
      testScope = scope;
      isCustomScope = false;
    }
  });

  if (!isCustomScope) {
    if (testScope.includes("default")) {
      isDefaultScope = true;
    }
  } else {
    testScope = customRuns;
  }

  allureReportDir = `../public/${runScope}/${testScope}`;
}

function copyDownloadedHistory() {
  console.log("Copy downloaded history:");
  fs.mkdirSync(`../${runScope}/${testScope}/allure-results/history`, {recursive: true});
  fs.mkdirSync(`../public`);

  if (fs.existsSync(`../${pastReport}/public`)) {
    execSync(`cp -r "../${pastReport}/public/." "../public"`);
    if (fs.existsSync(`${allureReportDir}/history`)) {
      execSync(`cp -r "${allureReportDir}/history/." "../${runScope}/${testScope}/allure-results/history"`);
    } else {
      console.log(`Past report wasn't found for ${runScope} on ${testScope} scope, generating report from existing results`);
    }
  } else {
    console.log(`Past report results wasn't found, generating report from existing results`);
  }

  execSync(`cp -r "../allure-results/." "../${runScope}/${testScope}/allure-results"`);
}

function generateAllureReport() {
  console.log(`Removing old report for ${runScope}'s ${testScope}`);
  fs.rmdirSync(`${allureReportDir}`, {recursive: true});
  fs.mkdirSync(`${allureReportDir}`, {recursive: true});

  console.log('Adding custom issues categories');
  fs.copyFileSync("categories.json", `../${runScope}/${testScope}/allure-results/categories.json`);

  console.log("Generating and copying allure report to corresponding directory of gitlab pages folder");
  execSync(`allure generate "../${runScope}/${testScope}/allure-results" -o "../${runScope}/${testScope}/allure-report"`);
  execSync(`cp -R "../${runScope}/${testScope}/allure-report/." "${allureReportDir}"`);
}

function modifyAllureReport() {
  // update report name
  let summaryJson = JSON.parse(fs.readFileSync(`${allureReportDir}/widgets/summary.json`));
  summaryJson["reportName"] = `Cypress Integration ${runScope} Report`;
  fs.writeFileSync(`${allureReportDir}/widgets/summary.json`, JSON.stringify(summaryJson, null, 4));

  // rename both Behaviors tab to Test Cases and widget on the main page
  let behaviorsPlugin = fs.readFileSync(`${allureReportDir}/plugins/behaviors/index.js`).toString();
  behaviorsPlugin = behaviorsPlugin.replace("name: 'Behaviors'", "name: 'Test Cases'")
    .replace("name: 'Features by stories'", "name: 'All Tests'");
  fs.writeFileSync(`${allureReportDir}/plugins/behaviors/index.js`, behaviorsPlugin);

  // remove useless for us Packages tab with same data as in Behaviors/Test Cases, skip this step if you need it
  fs.rmdirSync(`${allureReportDir}/plugins/packages`, {recursive: true});

  // renaming main app tabs and elements
  let allureAppJs = fs.readFileSync(`${allureReportDir}/app.js`).toString();
  allureAppJs = allureAppJs.replace(/"Categories"/g, '"Failures"')
    .replace(/"Graphs"/g, '"Graph Analytics"')
    .replace(/"Suites"/g, '"Test Suites"')
    .replace(/"Trend"/g, '"Execution Trend"')
    .replace(/"Timeline"/g, '"Timeline Slider"');
  fs.writeFileSync(`${allureReportDir}/app.js`, allureAppJs);

  // setting custom logo for report instead of default
  fs.copyFileSync("custom-logo.svg", `${allureReportDir}/plugins/custom-logo/custom-logo.svg`)
}

function updateReportHtml() {
  const defaultIndexHtml = "../public/index.html";
  const scopedIndexHtml = `../public/${runScope}/index.html`;
  summaryJson = JSON.parse(fs.readFileSync(`${allureReportDir}/widgets/summary.json`));
  console.log("Updating report .html file with test-results of run:");
  if (summaryJson.statistic != null) {
    if (isDefaultScope) {
      console.log(`Updating both main page result for ${runScope} and result of ${testScope} run on ${runScope} page`);
      updateHtmlWithRunResults(defaultIndexHtml, runScope);
    }
    updateHtmlWithRunResults(scopedIndexHtml, testScope);
  } else {
    throw new Error("Statistic for current generated report can't be found");
  }
}

function updateHtmlWithRunResults(htmlPath, scope) {
  const testEntries = ["total", "passed", "failed", "skipped", "broken", "duration", "start", "stop"];

  let dom;
  let inputHtml;

  try {
    inputHtml = fs.readFileSync(htmlPath, "utf8");
  } catch (error) {
    if (error.code !== "ENOENT") {
      throw console.log(error);
    }
    console.log(`Report .html file ${htmlPath} from previous run wasn't found, template will be used:`);
    generateHtml(htmlPath, scope);
    inputHtml = fs.readFileSync(htmlPath, "utf8");
  }

  // upgrading the report html using the report_template
  if (!inputHtml.includes(versionId)) {
    generateHtml(htmlPath, scope);
    inputHtml = fs.readFileSync(htmlPath, "utf8");
  }
  dom = new JSDOM(inputHtml);


  let testData;
  testEntries.forEach(testEntry => {
    if (testEntry === "duration") {
      let duration = summaryJson["time"][testEntry];
      let minutes = Math.floor(duration / 60000);
      let seconds = ((duration % 60000) / 1000).toFixed(0);
      testData = `Duration: ${minutes}m:${seconds}s`;
    } else if (testEntry === "start" || testEntry === "stop") {
      testData = summaryJson["time"][testEntry];
      let data = testEntry + ": " + (new Date(testData)).toLocaleString("en-GB", {timeZone: "Europe/Kiev"}).toString();
      testData = data.charAt(0).toUpperCase() + data.slice(1);
    } else {
      testData = summaryJson["statistic"][testEntry];
      if (testEntry === "failed" && testData == 0) {
        const startDate = (new Date(summaryJson["time"]["start"]))
            .toLocaleString("en-GB", {timeZone: "Europe/Kiev"}).toString();
        dom.window.document.getElementById(`${scope}-lastSuccess`).textContent = "Pipeline ID: " + envJson.PIPELINE_NUMBER;
        dom.window.document.getElementById(`${scope}-lastSuccessLink`).setAttribute("href", envJson.PIPELINE_URL);
        dom.window.document.getElementById(`${scope}-lastSuccessStart`).textContent = "Started At: " + startDate;
      }
    }
    dom.window.document.getElementById(`${scope}-${testEntry}`).textContent = testData;
  });
  dom.window.document.getElementById(`${scope}-lastRun`).setAttribute("href", envJson.PIPELINE_URL);
  fs.writeFileSync(htmlPath, dom.window.document.documentElement.outerHTML);
}

function generateHtml(copyPath, scope) {
  // generating the html with proper element ids and links to reports
  console.log(`Generating the ${scope} .html from template due to new version ${versionId}`);
  const reportTemplatePath = "./report_template.html";

  // cloning rows
  fs.copyFileSync(reportTemplatePath, copyPath);
  let inputHtml = fs.readFileSync(copyPath, "utf8");
  let dom = new JSDOM(inputHtml);
  const scopeList = (scope === runScope) ? runScopes : testScopes;
  for (let i = 0; i < scopeList.length - 1; i++) {
    let resultsRow = dom.window.document.querySelector("tr:nth-of-type(2)").cloneNode(true);
    dom.window.document.querySelector("tbody").appendChild(resultsRow);
  }

  fs.writeFileSync(copyPath, dom.window.document.documentElement.outerHTML);
  inputHtml = fs.readFileSync(copyPath, "utf8");
  dom = new JSDOM(inputHtml);

  // set element navigation links and element ids
  for (let i = 0; i < scopeList.length; i++) {
    dom.window.document.getElementsByName("scopeLink").item(i).setAttribute("href", scopeList[i]);
    dom.window.document.getElementsByName("scopeName").item(i).textContent = scopeList[i];

    let idsNumber = dom.window.document.querySelectorAll(`tr:nth-of-type(${i + 2}) [id]`).length;
    for (let j = 0; j < idsNumber; j++) {
      let id = dom.window.document.querySelectorAll(`tr:nth-of-type(${i + 2}) [id]`)[j].getAttribute("id");
      dom.window.document.querySelectorAll(`tr:nth-of-type(${i + 2}) [id]`)[j]
        .setAttribute("id", scopeList[i] + id);
    }
  }
  fs.writeFileSync(copyPath, dom.window.document.documentElement.outerHTML);
}
