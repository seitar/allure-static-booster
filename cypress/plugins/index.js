/// <reference types="cypress" />
/// <reference types="@shelex/cypress-allure-plugin" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************
const AllureWriter = require('@shelex/cypress-allure-plugin/writer');
// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
    config.env.REGRESSION = config.env.REGRESSION || "all";
    config.env.ENV = config.env.ENV || "dev";
    config.env.EXPERIMENT = config.env.EXPERIMENT || "Default";
    config.env.PIPELINE_NUMBER = config.env.PIPELINE_NUMBER || "local";
    config.env.PIPELINE_URL = config.env.PIPELINE_URL || "local";
    // `on` is used to hook into various events Cypress emits
    // `config` is the resolved Cypress config
    AllureWriter(on, config);
    return config;
};

