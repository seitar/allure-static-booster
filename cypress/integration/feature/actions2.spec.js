context('ActionsFeature', () => {
    before(() =>{
        cy.visit('https://docs.cypress.io/api/commands/and');
    });
    context('Actionsss2', () => {

        it('.focus() - focus on a DOM element2', () => {
            cy.allure()
                .tms('docs', 'https://on.cypress.io/focus')
                .severity('minor');
            cy.get('.action-focus')
                .focus()
                .should('not.have.class', 'focus')
                .prev()
                .should('have.attr', 'style', 'color: red;');
        });
    });

    context('skip', () => {
        it.skip('skipped test', () => {
        });
    });
});
