context('ActionsSmoke', () => {
    before(() =>{
        cy.visit('https://docs.cypress.io/api/commands/and');
    });
    context('actions', () => {

        it('click', () => {
            cy.get("body");
        });
    });

    context('blur', () => {
        it('.blur() - blur off a DOM element', () => {
            cy.allure().tms('docs', 'https://on.cypress.io/blur');
            cy.get('.action-blur')
                .type('About to blur')
                .blur()
                .should('have.class', 'error')
                .prev()
                .should('have.attr', 'style', 'color: red;');
        });
    });
});
