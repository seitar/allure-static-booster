 # About
 Solution for generating self-hosted Allure report on GitLab pages including the tables with results, pipeline links and navigation between the different Allure reports.
  
 Solution is not language specific and need only valid results(with `environment.properties` file included) to generate valid Allure report 
 
 All solution code is placed at `allure-static-booster` folder
 
 Reports storage are here:
   - https://seitar.gitlab.io/allure-static-booster/
 
 ## How it works and run
All build flow described at `gitlab-ci.yml`.
Jobs run on the existing in this projecr registry images - $CYPRESS_IMAGE for test execution job and $NODE_JAVA_IMAGE for reporting and download history job. We run `docker` build and push job only on pushes in `docker-branch` branch to build the new predefined images. All build and install info is in gitlab.ci.yml and Dockerfiles at `docker` folder.

So the first flow is run `allure-static-booster/download-test-history.js` script from `download test history` job in parallel with test - it will find the latest success pages job for `master` branch and download it. Job will fail if no pipelines were found but it will not affect the `report` job which will generate report from existing allure results in the way of no found the history for this run and test scope.

`pages` report job starts after test run and consists in calling the `allure-static-booster/download-test-history.js` which describe test scope, generate allure report, update/generate html table with report links and test run results 
 
 ## Requirements
Execution image with node, java, allure and unzip installed(see example at - `docker/node_java/Dockerfile`), node libs: jsdom, jsonpath, syncRequest

Also you will need to setup your GitLab project access token or personal access token to pass $CI_PROJECT_TOKEN as CI variable  to `download test history` job.
To define your report table structure on the `pages` job site - please fill test_scopes.json, using requirements about run and test scopes below:
 
 ### Run and test scopes
 - Run scopes which contains results history scopes for different runs are hosted at `allure-static-booster/test_scopes.json`
 - `runScopes` object keys("smoke", "feature", "all", etc) are the same as $REGRESSION variable values for `test` CI jobs
 - objects inside `test_scopes.json`["runScopes"][$REGRESSION] are the key value pairs for test scopes,
  where object name is both a link and folder name of allure report; and value is modifications for fields of `test_scopes.json`["default"] object which is using to describing test scope
 - all fields at the `test_scopes.json`["default"] object and its modifiers are in the same format as pre-defined on CI side
 - test scope ("prod(default)", "dev(default)") which contain "default" in his string also updates the values on main page
 - all test runs with CI variables different from described at `test_scopes.json` will go to the `custom_runs` scope which is added automatically
 - all node names should consist of only one word, use '_' istead of ' '
 - to update your allure reports table on Gitlab Pages after updating the `test_scopes.json` - add execSync(\`rm -rf "public/${runScope}"\`); call  in `update-run-results.js` at `generateAllureReport()` function before the allure report generation and remove it after successful run of your reporting job for `master` branch
 
 ## Notes
  - Several unimportant allure customizing steps like an adding custom defects categories, changing logo, report or tab names are in function `modifyAllureReport()` at `update-run-results.js`
  - Variables at `.test_template`  in `gitlab-ci.yml` started with `CYPRESS_` prefix due to cypress env variables resolve process.
  - In case you want to update the report-template - it hosted at `allure-static-booster/report_template.html`, after update you will need to change the `id` value of `header` tag and update correspondingly the `versionId` value at `update-run-results.js`.
